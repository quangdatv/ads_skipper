# Ads skipper
This chrome/opera extension help you to skip ads waiting for various URL shortener sites. However, it **cannot help you solve CAPTCHA**.

## Supported sites

Currently, those sites are supported:

- adf.ly, j.gs, q.gs, ay.gy
- ouo.io, ouo.press
- short.am

## Installation
This project is still under development and not available in Chrome store. You must install this extension in developer mode. Please see [this page][1] for detailed instruction.

## Screenshots
Please see [this page][2] for screenshots

[1]: https://bitbucket.org/quangdatv/ads_skipper/wiki/Installation.md
[2]: https://bitbucket.org/quangdatv/ads_skipper/wiki/Screenshots.md