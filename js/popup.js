function init() {
	chrome.runtime.sendMessage({action: "getPopupInfo"}, function(response) {
		setSkipCountDisplay(response.skipCount);
		setLastSkipDisplay(response.lastSkip);
	});
	// clear Counter button
	var clearCounterButton = document.querySelector("#clear-counter");
	clearCounterButton.addEventListener("click", function() {
		chrome.runtime.sendMessage({action: "clearCounter"}, function(response) {
			setSkipCountDisplay(0);
			setLastSkipDisplay(null);
		});
	});
}
function setSkipCountDisplay(skipCount) {
    var skipCountSpan = document.querySelector('#skip-count');
	
	if(!skipCount || skipCount === null || skipCount < 0) {
		skipCount = 0;
	}
	
	skipCountSpan.innerHTML = skipCount;
}

function setLastSkipDisplay(lastSkip) {
    var lastSkipSpan = document.querySelector('#last-skip');

    if(!lastSkip || lastSkip === null || lastSkip == "") {
		lastSkipSpan.innerHTML = "---";
	}
	else {
		lastSkipSpan.innerHTML = "<a href='" + lastSkip + "'>" + lastSkip + "</a>";
	}
}

document.addEventListener('DOMContentLoaded', init);