if(!localStorage.skipCount || localStorage.skipCount === null || localStorage.skipCount < 0) {
	localStorage.skipCount = 0;
}

if(!localStorage.lastSkip || localStorage.lastSkip === null || localStorage.lastSkip === "") {
	localStorage.lastSkip = "";
}

chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		switch (request.action) {
			case "incrementSkipCount":
				localStorage.skipCount++;
				localStorage.lastSkip = request.skipFrom;
				break;
			case "clearCounter":
				localStorage.skipCount = 0;
				localStorage.lastSkip = "";
				sendResponse();
				break;
			case "getPopupInfo":
				sendResponse({
					skipCount: localStorage.skipCount,
					lastSkip: localStorage.lastSkip
				});
				break;
		}		
});