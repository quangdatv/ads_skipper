(function() {
	if (typeof ysmm === 'undefined') {
		// no code
	} else {
		// crack code to find url
		var N = '';
		var M = '';
		
		for(var i = 0; i < ysmm.length; i++) {
			if(i%2 == 0) {
				N += ysmm[i];
			} else {
				M = ysmm[i] + M;
			}
		}
		
		var key = N + M;

		var url = window.atob(key).substring(2);

		if(url && url !== "") {
			window.onbeforeunload = function(evt) {return;}
			window.location = url;
		}
	}
})();
